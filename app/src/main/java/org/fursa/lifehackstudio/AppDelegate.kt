package org.fursa.lifehackstudio

import android.app.Application
import org.fursa.lifehackstudio.di.AppComponent
import org.fursa.lifehackstudio.di.DaggerAppComponent
import org.fursa.lifehackstudio.di.NetworkModule

class AppDelegate : Application() {

    override fun onCreate() {
        super.onCreate()

        injector = DaggerAppComponent
            .builder()
            .networkModule(NetworkModule())
            .build()
        injector.inject(this);
    }

    companion object {
        lateinit var injector: AppComponent
    }
}