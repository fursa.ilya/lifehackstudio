package org.fursa.lifehackstudio.network

import android.util.Log
import android.widget.Toast
import com.google.gson.annotations.SerializedName

data class Company(
    val id: String,
    val name: String,
    @SerializedName("img")
    val imgUrl: String
) {
    override fun toString(): String {
        return "Company(id='$id', name='$name', imgUrl='$imgUrl')"
    }

}