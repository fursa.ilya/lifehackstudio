package org.fursa.lifehackstudio.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiCall {
    @GET("test.php")
    fun getCompanies(): Single<List<Company>>

    @GET("test.php")
    fun getCompanyById(@Query("id") id: Int): Single<List<Details>>
}