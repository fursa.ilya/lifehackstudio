package org.fursa.lifehackstudio.network

import com.google.gson.annotations.SerializedName

data class Details(
    val id: Int,
    val name: String,
    @SerializedName("img")
    val imgUrl: String,
    val description: String,
    val lat: Double,
    val lon: Double,
    @SerializedName("www")
    val website: String,
    val phone: String
) {
    override fun toString(): String {
        return "Details(id=$id, name='$name', description='$description', lat=$lat, lon=$lon, website='$website', phone='$phone')"
    }
}