package org.fursa.lifehackstudio.utils

import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso
import org.fursa.lifehackstudio.Const
import org.fursa.lifehackstudio.Const.BASE_URL
import org.fursa.lifehackstudio.R

@BindingAdapter(value = ["imageUrl"])
fun ImageView.loadImage(imageUrl: String?) {
    Picasso.get()
        .load("${BASE_URL}${imageUrl}")
        .into(this)

    Log.d("Img", "${BASE_URL}${imageUrl}")
}

@BindingAdapter(value = ["backUrl"])
fun ImageView.loadBackground(imageUrl: String?) {
    Picasso.get()
        .load("${BASE_URL}${imageUrl}")
        .into(this)

    Log.d("Img", "${BASE_URL}${imageUrl}")
}
