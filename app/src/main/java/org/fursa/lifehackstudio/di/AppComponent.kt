package org.fursa.lifehackstudio.di

import dagger.Component
import org.fursa.lifehackstudio.AppDelegate
import org.fursa.lifehackstudio.MainActivity
import org.fursa.lifehackstudio.ui.MainViewModel
import javax.inject.Singleton

@Singleton
@Component(
    modules = [NetworkModule::class]
)
interface AppComponent {
    fun inject(app: AppDelegate)
    fun inject(activity: MainActivity)
    fun inject(viewModel: MainViewModel)
}