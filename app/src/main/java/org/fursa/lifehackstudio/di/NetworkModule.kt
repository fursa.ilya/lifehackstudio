package org.fursa.lifehackstudio.di

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.fursa.lifehackstudio.Const
import org.fursa.lifehackstudio.network.ApiCall
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    private val logger = HttpLoggingInterceptor()
    private var okHttpClient: OkHttpClient
    private var retrofit: Retrofit

    init {
        logger.level = HttpLoggingInterceptor.Level.BODY

        okHttpClient = OkHttpClient.Builder()
            .addInterceptor(logger)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(Const.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofitClient(): ApiCall {
        return retrofit.create(ApiCall::class.java)
    }
}