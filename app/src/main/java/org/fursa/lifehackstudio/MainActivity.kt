package org.fursa.lifehackstudio

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import org.fursa.lifehackstudio.network.ApiCall
import org.fursa.lifehackstudio.ui.MainViewModel
import org.fursa.lifehackstudio.ui.adapter.CompanyAdapter
import org.fursa.lifehackstudio.ui.details.FullscreenDetailsDialogFragment
import javax.inject.Inject

class MainActivity : AppCompatActivity(), CompanyAdapter.OnCompanyClickListener, SwipeRefreshLayout.OnRefreshListener {
    private val companyAdapter = CompanyAdapter()
    private val linearLayoutManager = LinearLayoutManager(this)
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this)[MainViewModel::class.java]

        recyclerViewCompany.apply {
            adapter = companyAdapter
            layoutManager = linearLayoutManager
        }

        companyAdapter.apply {
            setCompanyListener(this@MainActivity)
        }

        refresher.setOnRefreshListener(this)

    }

    override fun onStart() {
        super.onStart()
        viewModel.doLoad()
    }

    override fun onResume() {
        super.onResume()

        viewModel.getCompanies().observe(this, Observer { companies ->
            companyAdapter.setDataSource(companies.toMutableList())
        })

        viewModel.getError().observe(this, Observer { error ->
            Toast.makeText(this, error, Toast.LENGTH_LONG).show()
        })


    }

    override fun onCompanySelectedListener(id: Int) {
        val dialog = FullscreenDetailsDialogFragment.newInstance(id)
        dialog.show(supportFragmentManager, dialog.tag)
    }

    override fun onRefresh() {
        viewModel.doLoad()
        refresher.isRefreshing = false
    }

}
