package org.fursa.lifehackstudio.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import org.fursa.lifehackstudio.R
import org.fursa.lifehackstudio.databinding.ItemCompanyBinding
import org.fursa.lifehackstudio.network.Company


class CompanyAdapter: RecyclerView.Adapter<CompanyAdapter.CompanyViewHolder>() {
    private var dataSource = mutableListOf<Company>()
    private var listener: OnCompanyClickListener? = null

    fun setDataSource(dataSource: MutableList<Company>) {
        this.dataSource = dataSource;
        notifyDataSetChanged()
    }

    fun setCompanyListener(listener: OnCompanyClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyViewHolder {
        val binding: ItemCompanyBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_company, parent, false
        )

        return CompanyViewHolder(binding)
    }

    override fun getItemCount() = dataSource.count()

    override fun onBindViewHolder(holder: CompanyViewHolder, position: Int) {
        val company = dataSource[position]
        holder.bind(company)
    }

    inner class CompanyViewHolder(private val binding: ItemCompanyBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(company: Company) {
            Log.d("Image", "img url is ${company.imgUrl}")
            binding.company = company
            binding.executePendingBindings()

            itemView.setOnClickListener { view ->
                listener?.onCompanySelectedListener(company.id.toInt())
            }
        }
    }

    interface OnCompanyClickListener {
        fun onCompanySelectedListener(id: Int)
    }
}