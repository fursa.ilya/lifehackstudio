package org.fursa.lifehackstudio.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.fursa.lifehackstudio.AppDelegate
import org.fursa.lifehackstudio.network.ApiCall
import org.fursa.lifehackstudio.network.Company
import org.fursa.lifehackstudio.network.Details
import javax.inject.Inject

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val companies =  MutableLiveData<List<Company>>()
    private val details = MutableLiveData<List<Details>>()
    private val disposable = CompositeDisposable()
    private val error = MutableLiveData<String>()

    @Inject
    lateinit var apiCall: ApiCall

    init {
        AppDelegate.injector.inject(this)
    }

    fun doLoad() {
        disposable.add(
            apiCall.getCompanies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    companies.value = it
                }, {
                    error.value = it.localizedMessage
                })
        )
    }

    fun loadDetails(id: Int) {
        disposable.add(
            apiCall.getCompanyById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    details.value = it
                }, {
                    error.value = it.localizedMessage
                })
        )
    }

    fun getCompanies() = companies

    fun getError() = error

    fun getDetails() = details

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}