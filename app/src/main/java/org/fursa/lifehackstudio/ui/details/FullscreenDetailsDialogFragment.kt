package org.fursa.lifehackstudio.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_details.*
import org.fursa.lifehackstudio.R
import org.fursa.lifehackstudio.databinding.FragmentDetailsBinding
import org.fursa.lifehackstudio.ui.MainViewModel

class FullscreenDetailsDialogFragment : DialogFragment() {
    private lateinit var viewModel: MainViewModel

    companion object {
        fun newInstance(id: Int): FullscreenDetailsDialogFragment {
            val dialog = FullscreenDetailsDialogFragment()
            val args = Bundle().apply {
                putInt("id", id)
            }
            dialog.arguments = args
            return dialog
        }
    }

    override fun getTheme(): Int {
         return R.style.DialogTheme
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this)[MainViewModel::class.java]
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailsBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_details, container, false
        )
        val companyId = arguments?.getInt("id", 0)
        if (companyId != null) {
            viewModel.loadDetails(companyId)
        }

        viewModel.getDetails().observe(viewLifecycleOwner, Observer { details ->
            details.forEach { detail ->
                binding.details = detail
            }
        })

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationOnClickListener { dismiss() }
        collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(context!!.applicationContext, R.color.colorWhite))
    }

}